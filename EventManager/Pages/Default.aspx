﻿<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="../Scripts/Plugins/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/production.js"></script>

    <!-- Meta crap -->
    <meta name="WebPartPageExpansion" content="full" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Add your CSS styles to the following file -->
    <link href='https://fonts.googleapis.com/css?family=Crimson+Text' rel='stylesheet' type='text/css' />
    <link rel="Stylesheet" type="text/css" href="../Content/production.css" />
    <style type="text/css" media="print">
        @page {
            size: auto !important; /* auto is the current printer page size */
            margin: 0mm !important; /* this affects the margin in the printer settings */
        }
    </style>

</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    DNLTD Event Programme Manager
</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <SharePoint:ScriptLink Name="clienttemplates.js" runat="server" LoadAfterUI="true" Localizable="false" />
    <SharePoint:ScriptLink Name="clientforms.js" runat="server" LoadAfterUI="true" Localizable="false" />
    <SharePoint:ScriptLink Name="clientpeoplepicker.js" runat="server" LoadAfterUI="true" Localizable="false" />
    <SharePoint:ScriptLink Name="autofill.js" runat="server" LoadAfterUI="true" Localizable="false" />
    <SharePoint:ScriptLink Name="sp.RequestExecutor.js" runat="server" LoadAfterUI="true" Localizable="false" />
    <SharePoint:ScriptLink Name="sp.js" runat="server" LoadAfterUI="true" Localizable="false" />
    <SharePoint:ScriptLink Name="SP.UserProfiles.js" runat="server" LoadAfterUI="true" Localizable="false" />
    <SharePoint:ScriptLink Name="sp.runtime.js" runat="server" LoadAfterUI="true" Localizable="false" />
    <SharePoint:ScriptLink Name="sp.core.js" runat="server" LoadAfterUI="true" Localizable="false" />
    <br />
    <div class="app-body">
        <div class="wrapper">
            <div class="box">
                <%--<div class="row row-offcanvas row-offcanvas-left">--%>
                    <!-- main right col -->
                    <div class="column col-sm-12 col-xs-12" id="main">
                        <!-- top nav -->
                        <div class="navbar navbar-blue navbar-static-top">
                            <div class="navbar-header">
                                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <nav class="collapse navbar-collapse" role="navigation">
                        
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="#dashboard/my" data-toggle="collapse" data-target=".navbar-collapse"><i class="glyphicon glyphicon-user"></i> My Dashboard</a>
                      </li>
                        <li>
                        <a href="#dashboard/admin" data-toggle="collapse" data-target=".navbar-collapse"><i class="glyphicon glyphicon-dashboard"></i> Admin Dashboard</a>
                      </li>
                        <li>
                        <a href="#events" data-toggle="collapse" data-target=".navbar-collapse"><i class="glyphicon glyphicon-th-large"></i> Published Events</a>
                      </li>
                        <li>
                        <a href="#events/calendar" data-toggle="collapse" data-target=".navbar-collapse"><i class="glyphicon glyphicon-calendar"></i> Event Calendar</a>
                      </li>
                        <li>
                        <a href="" class="event-types" data-toggle="collapse" data-target=".navbar-collapse"><i class="glyphicon glyphicon-list"></i> Event Types</a>
                      </li>
                        <li>
                        <a href="#events/new" data-toggle="collapse" data-target=".navbar-collapse"><i class="glyphicon glyphicon-plus"></i> New Event</a>
                      </li>
                    </ul>
                    <div class="input-group input-group-sm ev-search pull-right" ng-controller="SearchBox">
                          <input type="text" class="form-control" placeholder="Search Events..." 
                              name="srch-term" id="srch-term" ng-model="searchTerms" />
                          <div class="input-group-btn">
                            <a class="btn btn-default" style="width:35px;" type="submit" ng-click="runSearch($event)">
                                <i class="glyphicon glyphicon-search"></i>
                            </a>
                          </div>
                        </div>
                  	</nav>
                        </div>
                        <!-- /top nav -->
                        <div class="padding">
                        <div class="full app-content-area">
                            <div growl></div>
                            <div ng-view></div>
                        </div>
                            </div>
                        <!-- /col-9 -->
                        <!-- /padding -->
                    </div>
                    <!-- /main -->
                </div>
            </div>
        </div>
    <%--</div>--%>
</asp:Content>
