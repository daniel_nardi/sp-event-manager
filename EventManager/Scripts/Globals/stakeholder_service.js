﻿var ListServices = angular.module("ListServices", []);
ListServices.service("StakeholderService", ["AngularSPCSOM", "$rootScope", function (csom, scope) {
    this.stakeholders = [];
    this.GetStakeholders = function () { //populates the initial stakeholders collection @ load for autocomplete
        var _this = this;
        return csom.GetListItems("Event Stakeholders", spWebUrl);
    };
    this.AddNewStakeholders = function (stakeholders, delim) { //adds new stakeholders from pick control
        if (stakeholders) {
            var _this = this;
            stakeholders = stakeholders.split(delim);
            var additional = _.reject(stakeholders, function (sh) {
                return _.some(_this.stakeholders, function (sh_col) {
                    return sh.toLowerCase() == sh_col.toLowerCase();
                })
            });
            if (additional.length > 0) {
                this.stakeholders = this.stakeholders.concat(additional); //add new ones to singleton object

                var batchData = [];
                _.each(additional, function (sh) {
                    batchData.push({ Title: sh })
                })
                //insert new stakeholders into SP
                csom.BulkCreateListItems("Event Stakeholders", spWebUrl, batchData).then(function () {
                    console.log("New stakeholders added successfully");
                    scope.$broadcast('stakeholders.updateComplete');
                });
            }
        }
        else console.log("No stakeholders to add.")
    }
}])