﻿ListServices.service("SettingService", ["AngularSPCSOM", "$rootScope", "$q", function (csom, scope, $q) {
    this.settings = {};

    this.GetSettings = function () {
        var _this = this, deff = $q.defer();
        if (_.isEmpty(this.settings)) {
            csom.GetListItems("Settings", spWebUrl).then(function (data) {
                if (data.length > 0) {
                    _.each(data, function (item) {
                        _this.settings[item.Title] = { ID: item.ID, Value: item.SettingValue };
                    })
                }
                deff.resolve(_this.settings);
            });
        }
        else deff.resolve(this.settings);

        return deff.promise;
    }

    this.AddSetting = function (key, value) {
        var _this = this, deff = $q.defer();
        var _setting = {
            Title: (key) ? key : "",
            SettingValue: (value) ? value : ""
        }
        csom.CreateListItem("Settings", spWebUrl, _setting).then(function (item) {
            _this.settings[item.Title] = { ID: item.ID, Value: item.SettingValue };
            deff.resolve(_this.settings)
        });
        return deff.promise;
    }

    this.UpdateSetting = function (key, value) {
        var id, _this = this, deff = $q.defer();
        (_this.settings[key]) ? id = _this.settings[key].ID : id = null;
        if (id) {
            csom.UpdateListItem(id, "Settings", spWebUrl, { Title: key, SettingValue: value }).then(function (item) {
                _this.settings[key] = item.SettingValue;
                deff.resolve(_this.settings);
            });
        }
        return deff.promise;
    }
}])
.service("TypeService", ["AngularSPCSOM", "$rootScope", "$q", function (csom, scope, $q) {
    this.eventTypes = [];

    this.GetEventTypes = function () {
        var _this = this, deff = $q.defer();

        if (_this.eventTypes.length == 0) {
            csom.GetListItems("Event Types", spWebUrl).then(function (data) {
                _this.eventTypes = data;
                deff.resolve(_this.eventTypes);
            })
        }
        else deff.resolve(_this.eventTypes);

        return deff.promise;
    }
}])

/*angular-modal-service v0.6.9 - https://github.com/dwmkerr/angular-modal-service */
!function () { "use strict"; var e = angular.module("angularModalService", []); e.factory("ModalService", ["$animate", "$document", "$compile", "$controller", "$http", "$rootScope", "$q", "$templateRequest", "$timeout", function (e, n, t, r, l, o, c, u, i) { function a() { var n = this, l = function (e, n) { var t = c.defer(); return e ? t.resolve(e) : n ? u(n, !0).then(function (e) { t.resolve(e) }, function (e) { t.reject(e) }) : t.reject("No template or templateUrl has been specified."), t.promise }, a = function (n, t) { var r = n.children(); return r.length > 0 ? e.enter(t, n, r[r.length - 1]) : e.enter(t, n) }; n.showModal = function (n) { var u = c.defer(), p = n.controller; return p ? (l(n.template, n.templateUrl).then(function (l) { var p = o.$new(), f = c.defer(), d = c.defer(), m = { $scope: p, close: function (n, t) { (void 0 === t || null === t) && (t = 0), i(function () { f.resolve(n), e.leave($).then(function () { d.resolve(n), p.$destroy(), m.close = null, u = null, f = null, g = null, m = null, $ = null, p = null }) }, t) } }; n.inputs && angular.extend(m, n.inputs); var v = t(l), $ = v(p); m.$element = $; var h = r(n.controller, m); n.controllerAs && (p[n.controllerAs] = h), n.appendElement ? a(n.appendElement, $) : a(s, $); var g = { controller: h, scope: p, element: $, close: f.promise, closed: d.promise }; u.resolve(g) }).then(null, function (e) { u.reject(e) }), u.promise) : (u.reject("No controller has been specified."), u.promise) } } var s = n.find("body"); return new a }]) }();
//# sourceMappingURL=angular-modal-service.min.js.map