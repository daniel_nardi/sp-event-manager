﻿var _appGlobals = {}, _spSiteUserInfo = {}, _spAppSettings = {};
/*
*************************
SET UP GENERIC, NON DEPENDANT VARIABLES
*************************
*/
_appGlobals.EventFields = [
    "Title",
    "Agenda",
    "EventSummary",
    "EventPurpose",
    "Deliverables",
    "Schedule",
    "Stakeholders",
    "HostInformation",
    "VenueInformation",
    "EventStart",
    "EventEnd",
    "LeadParticipant",
    "AdditionalParticipants",
    "NominatedApprovers",
    "ParentEvent",
    "EventType",
    "EventStatus",
    "ParentLocation"
]

/*
*************************
SET UP GENERIC, NON DEPENDANT FUNCTIONS
*************************
*/
_appGlobals.GetAcceptableFormFields = function (context) {
    var results = [];
    _.each($(context).find('input, textarea').not('input[type="submit"]'), function (el) {
        var name = $(el).attr('name');
        if (name && name.substring(0,1) != "_") {
            results.push(name);
        }
    })
    return results;
}

_appGlobals.Loading = function(show, text){
    if (show) {
        //spinner
        $('body').loadingOverlay({
            loadingClass: 'loading',
            overlayClass: 'loading-overlay',
            spinnerClass: 'loading-spinner',
            iconClass: 'loading-icon',
            textClass: 'loading-text',
            loadingText: (text) ? text : 'loading'
        });
        $('.loading-overlay').css('height', $(window).height() + 'px');
    }
    else {
        $('body').loadingOverlay('remove', {
            loadingClass: 'loading',
            overlayClass: 'loading-overlay'
        });
    }
}

_appGlobals.SendEmail = function (from, to, subject, body) {
    var urlTemplate = spWebUrl + "/_api/SP.Utilities.Utility.SendEmail";
    $.ajax({
        contentType: 'application/json',
        url: urlTemplate,
        type: "POST",
        data: JSON.stringify({
            'properties': {
                '__metadata': { 'type': 'SP.Utilities.EmailProperties' },
                'From': from,
                'To': { 'results': [to] },
                'Body': body,
                'Subject': subject
            }
        }
      ),
        headers: {
            "Accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val()
        },
        success: function (data) {
            console.log('Email sent successfully')
        },
        error: function (err) {
            console.log(JSON.stringify(err));
        }
    });
}
