﻿ListServices.service("EventService", ["AngularSPCSOM", "$rootScope", "$q", "SettingService", function (csom, scope, $q, settings) {
    this.allEvents = [];
    this.timestamp = moment();

    /********CRUD ACTIONS**********/
    this.GetEvents = function () {
        var _this = this;
        var deff = $q.defer();
        var query = new CamlBuilder().View().Query().Where().TextField("EventStatus").NotEqualTo("Deleted")
            .OrderBy('EventStart').ToString();
        settings.GetSettings().then(function (stngs) {
            var refreshInterval = (stngs.RefreshInterval) ? parseInt(stngs.RefreshInterval.Value) : 20;
            if (_this.allEvents.length == 0 || _this.timestamp.isBefore(moment().add(-Math.abs(refreshInterval), 'minutes'))) {
                csom.GetListItems("Event Programmes", spWebUrl, query).then(function (data) {
                    _this.allEvents = data;
                    _this.timestamp = moment();
                    deff.resolve(_this.allEvents);
                }, function (error) {
                    console.log(error.get_message());
                })
            }
            else {
                deff.resolve(_this.allEvents);
            }
        })
        
        return deff.promise;
    }

    this.GetEvent = function (id) {
        return _.find(this.allEvents, function (event) {
            return event.ID == id;
        })
    }

    this.EventAndSubs = function (id) {
        return _.filter(this.allEvents, function (event) {
            return event.ParentEvent == id || event.ID == id;
        })
    }

    this.GetSubEvents = function (id) {
        return _.filter(this.allEvents, function (event) {
            return event.ParentEvent == id;
        })
    }

    this.AddEvent = function (event) {
        var _this = this;
        var deff = $q.defer();
        csom.CreateListItem("Event Programmes", spWebUrl, event).then(function (evt) {
            _this.allEvents.push(evt);
            deff.resolve(evt);
        }, function (error) {
            console.log(error.get_message());
        })
        return deff.promise;
    }

    this.RemoveEvent = function (id) {
        var _this = this;
        var deff = $q.defer();
        csom.UpdateListItem(id, "Event Programmes", spWebUrl, {EventStatus: "Deleted"}).then(function () {
            _this.allEvents = _.reject(_this.allEvents, function (evt) {
                return evt.ID == id;
            });
            deff.resolve(_this.allEvents);
        }, function (error) {
            console.log(error.get_message());
        })

        return deff.promise;
    }

    this.UpdateEvent = function (id, saveObj) {
        var _this = this;
        var deff = $q.defer();
        csom.UpdateListItem(id, "Event Programmes", spWebUrl, saveObj).then(function (event) {
            var newEv = {};
            _this.allEvents = _.reject(_this.allEvents, function (evt) {
                if (evt.ID == id) {
                    newEv = _.extend(evt, event);
                }
                return evt.ID == id;
            });
            _this.allEvents.push(newEv);
            deff.resolve(newEv);
        }, function (error) {
            console.log(error.get_message());
        })
        
        return deff.promise;
    }

    /********FILTERS**********/
    this.AllPublishedEventsThisYear = function () {
        var retObj = {
            events: [],
            monthly: {}
        }
        _.each(this.allEvents, function (event) {
            if (!event.ParentEvent &&
                event.EventStatus == "Published" &&
                moment(event.EventStart).format("YYYY") == moment().format("YYYY")){
                retObj.events.push(event);
                var month = moment(event.EventStart).format("MMMM");
                if (retObj.monthly[month]) {
                    retObj.monthly[month] += 1;
                }
                else retObj.monthly[month] = 1;
            }
        })

        return retObj;
    }

    this.AllPublishedEventsThisMonth = function () {
        var retObj = {
            events: [],
            type: {},
            status: {}
        }
        _.each(this.allEvents, function (event) {
            if (moment(event.EventStart).format("MMMM") == moment().format("MMMM") &&
                !event.ParentEvent) {
                retObj.events.push(event);
                var type = event.EventType, status = event.EventStatus;

                if (event.EventStatus == "Published") {
                    if (retObj.type[type]) {
                        retObj.type[type] += 1;
                    }
                    else retObj.type[type] = 1;
                }
                
                if (retObj.status[status]) {
                    retObj.status[status] += 1;
                }
                else retObj.status[status] = 1;
            }
        })

        return retObj;
    }

    this.CurrentEvents = function () {
        var retObj = {
            events: [],
            week: [],
            monthly: {}
        }
        _.each(this.allEvents, function (event) {
            if (!event.ParentEvent &&
                event.EventStatus == "Published") {
                retObj.events.push(event);
                if (moment(event.Modified).isAfter(moment().add(-7, 'days'))) {
                    retObj.week.push(event);
                }
                
            }
        });
        return retObj;
    }

    this.MyDraftEvents = function () {
        var retObj = {
            events: [],
            week: []
        };
        _.each(this.allEvents, function (event) {
            if (event.EventStatus == "Draft" &&
                event.Author.get_lookupId() == _spPageContextInfo.userId &&
                !event.ParentEvent) {

                retObj.events.push(event);
                if (moment(event.Created).isAfter(moment().add(-7, 'days'))) {
                    retObj.week.push(event);
                }
            }
        })
        return retObj;
    }

    this.MyCurrentEvents = function () {
        var retObj = {
            events: [],
            week: []
        };
        _.each(this.allEvents, function (event) {
            if (event.EventStatus == "Published" &&
                event.Author.get_lookupId() == _spPageContextInfo.userId &&
                moment(event.EventEnd).isAfter(moment()) &&
                !event.ParentEvent) {

                retObj.events.push(event);
                if (moment(event.Modified).isAfter(moment().add(-7, 'days'))) {
                    retObj.week.push(event);
                }
            }
        })
        return retObj;
    }

    this.CurrentEventsInvolvingMe = function () {
        return _.filter(this.allEvents, function (event) {
            var participant = _.some(event.AdditionalParticipants, function (part) {
                return part.get_lookupId() == _spPageContextInfo.userId;
            })
            return event.EventStatus == "Published" &&
                !event.ParentEvent &&
                ((event.LeadParticipant && event.LeadParticipant.get_lookupId() == _spPageContextInfo.userId) || participant)
        })
    }

    this.MyUpcomingEvents = function () {
        var retObj = {
            events: [],
            me: []
        };
        _.each(this.allEvents, function (event) {
            var participant = _.some(event.AdditionalParticipants, function(part){
                return part.get_lookupId() == _spPageContextInfo.userId;
            })
            if (moment(event.EventStart).isBefore(moment().add(30, 'days')) &&
                moment(event.EventStart).isAfter(moment()) &&
                !event.ParentEvent &&
                event.EventStatus == "Published" &&
                ((event.LeadParticipant && event.LeadParticipant.get_lookupId() == _spPageContextInfo.userId) || participant)
                ) {
                retObj.events.push(event);
                if (event.LeadParticipant && event.LeadParticipant.get_lookupId() == _spPageContextInfo.userId) {
                    retObj.me.push(event)
                }
            }
        });
        return retObj;
    }

    this.MyEventsToApprove = function () {
        var retObj = {
            events: [],
            overdue: []
        };

        _.each(this.allEvents, function (event) {
            var valid = false;
            valid = _.some(event.NominatedApprovers, function (approver) {
                return approver.get_lookupId() == _spPageContextInfo.userId
            })
            if (event.EventStatus == "Pending Approval" &&
                valid &&
                moment(event.EventEnd).isAfter(moment()) &&
                !event.ParentEvent) {
                retObj.events.push(event);
                if (moment(event.Modified).isBefore(moment().add(-3, 'days'))) {
                    retObj.overdue.push(event);
                }
            }
        })
        return retObj;
    }

    this.AllUpcomingEvents = function () {
        var retObj = {
            events: [],
            week: []
        };
        _.each(this.allEvents, function (event) {
            if (moment(event.EventStart).isBefore(moment().add(30, 'days')) &&
                moment(event.EventStart).isAfter(moment()) &&
                !event.ParentEvent &&
                event.EventStatus == "Published") {
                retObj.events.push(event);
                if (moment(event.EventStart).isBefore(moment().add(7, 'days'))) {
                    retObj.week.push(event)
                }
            }
        });
        return retObj;
    }

    this.AllCurrentDraftEvents = function () {
        var retObj = {
            events: [],
            week: []
        };
        _.each(this.allEvents, function (event) {
            if (event.EventStatus == "Draft" &&
                moment(event.EventEnd).isAfter(moment()) &&
                !event.ParentEvent) {
                retObj.events.push(event);
                if (moment(event.Created).isAfter(moment().add(-7, 'days'))) {
                    retObj.week.push(event);
                }
            }
        })
        return retObj;
    }

    this.AllCurrentEventsInReview = function () {
        var retObj = {
            events: [],
            rejected: []
        };
        _.each(this.allEvents, function (event) {
            if ((event.EventStatus == "Pending Approval" || event.EventStatus == "Rejected") &&
                moment(event.EventEnd).isAfter(moment()) &&
                !event.ParentEvent) {
                retObj.events.push(event);
                if (event.EventStatus == "Rejected") {
                    retObj.rejected.push(event);
                }
            }
        })
        return retObj;
    }

    this.PreviousEvents = function () {
        return _.filter(this.allEvents, function (event) {
            return moment(event.EventEnd).isBefore(moment()) &&
                event.EventStatus == "Published"
        })
    }

    this.RecentlyEndedEvents = function () {
        return _.filter(this.allEvents, function (event) {
            return moment(event.EventEnd).isBefore(moment()) &&
                moment(event.EventEnd).isAfter(moment().add(-30, "days")) &&
                event.EventStatus == "Published"
        })
    }

}])