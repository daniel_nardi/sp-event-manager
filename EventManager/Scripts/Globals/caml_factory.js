﻿var eventFactories = angular.module("EventManagerFactories", [])
.factory("EventCamlFactory", function () {
    var eventFactory = {};
    eventFactory.parentEventExpression = function () {
        return CamlBuilder.Expression().TextField("ParentEvent").IsNull().And().TextField("EventStatus").EqualTo("Published");
    };
    eventFactory.ParentEvents = function (baseQuery) { //top level events, pass to other queries as necessary
        var query = new CamlBuilder().View().Query().Where().TextField("ParentEvent").IsNull().OrderBy('EventStart').ToString();
        console.log("CAML EXECUTED: " + query);
        return query;
    };
    eventFactory.ChildEvents = function(parentId){ //child events, requires ID of parent event
        var query = new CamlBuilder().View().Query().Where().TextField("ParentEvent").EqualTo(parentId)
            .OrderBy('EventStart').ToString();
        console.log("CAML EXECUTED: " + query);
        return query;
    };
    eventFactory.OpenParentEvents = function () {
        var query = new CamlBuilder().View().Query().Where().TextField("ParentEvent").IsNull()
            .And().DateField("EventEnd").GreaterThanOrEqualTo(CamlBuilder.CamlValues.Today)
            .OrderBy('EventStart').ToString();
        console.log("CAML EXECUTED: " + query);
        return query;
    }
    eventFactory.MyParentEvents = function () {
        //Build new expression to hold combined OR statements
        var expr = CamlBuilder.Expression();

        //Build array to hold collection of OR statements
        var exprs = [];
        exprs.push(CamlBuilder.Expression().UserField("Author").EqualToCurrentUser());
        exprs.push(CamlBuilder.Expression().UserField("LeadParticipant").EqualToCurrentUser());
        exprs.push(CamlBuilder.Expression().UserField("AdditionalParticipants").ContainsCurrentUser());
        exprs.push(CamlBuilder.Expression().UserField("NominatedApprovers").ContainsCurrentUser());

        //Put all the OR statements together inside the Expression() object
        expr.Any.apply(expr, exprs);

        var query = new CamlBuilder().View().Query().Where().TextField("ParentEvent").IsNull()
            .And().All(expr).OrderBy('EventStart').ToString();
        console.log("CAML EXECUTED: " + query);
        return query;
    }
    eventFactory.MyDraftEvents = function () {
        var query = new CamlBuilder().View().Query().Where().TextField("EventStatus").EqualTo("Draft")
            .And().UserField("Author").EqualToCurrentUser()
            .OrderBy('EventStart').ToString();
        console.log("CAML EXECUTED: " + query);
        return query;
    };
    eventFactory.EventsForApproval = function () {
        var query = new CamlBuilder().View().Query().Where().TextField("EventStatus").EqualTo("Pending Approval")
            .And().UserField("NominatedApprovers").ContainsCurrentUser()
            .OrderBy('EventStart').ToString();
        console.log("CAML EXECUTED: " + query);
        return query;
    };
    eventFactory.EventAndSubordinates = function (parentId) {
        var query = new CamlBuilder().View().Query().Where().NumberField("ID").EqualTo(parentId)
            .Or().TextField("ParentEvent").EqualTo(parentId)
            .OrderBy('EventStart').ToString();
        console.log("CAML EXECUTED: " + query);
        return query;
    };
    eventFactory.MyEvents = function (baseQuery) { //events I am listed as a participant for
        var parentExpr = (baseQuery) ? baseQuery : null;
        
        //Build new expression to hold combined OR statements
        var expr = CamlBuilder.Expression();

        //Build array to hold collection of OR statements
        var exprs = [];
        exprs.push(CamlBuilder.Expression().UserField("LeadParticipant").EqualToCurrentUser());
        exprs.push(CamlBuilder.Expression().UserField("AdditionalParticipants").ContainsCurrentUser());
        exprs.push(CamlBuilder.Expression().UserField("NominatedApprovers").ContainsCurrentUser());

        //Put all the OR statements together inside the Expression() object
        expr.Any.apply(expr, exprs);

        //And OR collection expression to parent expression
        var finalExpr = expr;
        if (parentExpr) {
            finalExpr = CamlBuilder.Expression();
            finalExpr.All(parentExpr, expr);
        }

        var query = new CamlBuilder().View().Query().Where().All(finalExpr).OrderBy('EventStart').ToString();
        console.log("CAML EXECUTED: " + query);
        return query;
    };
    eventFactory.UpcomingEvents = function (baseQuery) { //events in the next 30 days
        var parentExpr = (baseQuery) ? baseQuery : null;

        //Build new expression to hold combined OR statements
        var expr = CamlBuilder.Expression();

        //Build array to hold collection of OR statements
        var exprs = [];
        exprs.push(CamlBuilder.Expression().DateField("EventStart").GreaterThanOrEqualTo(CamlBuilder.CamlValues.Today));
        exprs.push(CamlBuilder.Expression().DateField("EventStart").LessThanOrEqualTo(CamlBuilder.CamlValues.TodayWithOffset(30)));

        //Put all the OR statements together inside the Expression() object
        expr.All.apply(expr, exprs);

        //And OR collection expression to parent expression
        var finalExpr = expr;
        if (parentExpr) {
            finalExpr = CamlBuilder.Expression();
            finalExpr.All(parentExpr, expr);
        }
        
        var query = new CamlBuilder().View().Query().Where().All(finalExpr).OrderBy('EventStart').ToString();
        console.log("CAML EXECUTED: " + query);
        return query;
    };
    eventFactory.RecentlyEndedEvents = function (baseQuery) { //events ended in the last 30 days
        var parentExpr = (baseQuery) ? baseQuery : null;

        //Build new expression to hold combined OR statements
        var expr = CamlBuilder.Expression();

        //Build array to hold collection of OR statements
        var exprs = [];
        exprs.push(CamlBuilder.Expression().DateField("EventEnd").LessThanOrEqualTo(CamlBuilder.CamlValues.Today));
        exprs.push(CamlBuilder.Expression().DateField("EventEnd").GreaterThanOrEqualTo(CamlBuilder.CamlValues.TodayWithOffset(-30)));

        //Put all the OR statements together inside the Expression() object
        expr.All.apply(expr, exprs);

        //And OR collection expression to parent expression
        var finalExpr = expr;
        if (parentExpr) {
            finalExpr = CamlBuilder.Expression();
            finalExpr.All(parentExpr, expr);
        }

        var query = new CamlBuilder().View().Query().Where().All(finalExpr).OrderByDesc('EventEnd').ToString();
        console.log("CAML EXECUTED: " + query);
        return query;
    };
    eventFactory.EventsByStatus = function (status, baseQuery) { //events by status
        var parentExpr = (baseQuery) ? baseQuery : null;

        //Build new expression to hold combined OR statements
        var expr = CamlBuilder.Expression().TextField("EventStatus").EqualTo(status);

        //And OR collection expression to parent expression
        var finalExpr = expr;
        if (parentExpr) {
            finalExpr = CamlBuilder.Expression();
            finalExpr.All(parentExpr, expr);
        }

        var query = new CamlBuilder().View().Query().Where().All(finalExpr).OrderBy('EventStart').ToString();
        console.log("CAML EXECUTED: " + query);
        return query;
    };
    eventFactory.EventsByType = function (type, baseQuery) { //events by type
        var parentExpr = (baseQuery) ? baseQuery : null;

        //Build new expression to hold combined OR statements
        var expr = CamlBuilder.Expression().TextField("EventType").EqualTo(type);

        //And OR collection expression to parent expression
        var finalExpr = expr;
        if (parentExpr) {
            finalExpr = CamlBuilder.Expression();
            finalExpr.All(parentExpr, expr);
        }

        var query = new CamlBuilder().View().Query().Where().All(finalExpr).OrderBy('EventStart').ToString();
        console.log("CAML EXECUTED: " + query);
        return query;
    };
    return eventFactory;
});