﻿ListServices.service("AttendeeService", ["AngularSPCSOM", "$rootScope", "$q", "SettingService", function (csom, scope, $q, settings) {
    this.currentAttendee;
    /********CRUD ACTIONS**********/
    this.GetAttendees = function (eventId) {
        var _this = this;
        var query = new CamlBuilder().View().Query().Where().TextField("AssociatedEvent").EqualTo(eventId)
            .OrderBy('LastName').ToString();

        return csom.GetListItems("Event Attendees", spWebUrl, query);
    }

    this.GetAttendeesByName = function (name) {
        var _this = this;
        var query = new CamlBuilder().View().Query().Where().TextField("FirstName").Contains(name)
            .Or().TextField("LastName").Contains(name)
            .OrderBy('Created').ToString();

        return csom.GetListItems("Event Attendees", spWebUrl, query);
    }

    this.UpdateAttendance = function (id, attend) {
        var _this = this, deff = $q.defer(), saveObj = { AttendanceConfirmed: attend };
        csom.UpdateListItem(id, "Event Attendees", spWebUrl, saveObj).then(function (att) {
            deff.resolve(att);
        }, function (error) {
            console.log(error.get_message());
        })

        return deff.promise;
    }

    this.AddAttendee = function (attendee) {
        var _this = this;
        return csom.CreateListItem("Event Attendees", spWebUrl, attendee)
    }

    this.BulkAddAttendees = function (attendees) {
        return csom.BulkCreateListItems("Event Attendees", spWebUrl, attendees);
    }

    this.BulkRemoveAttendees = function (eventId) {
        var query = new CamlBuilder().View().Query().Where().TextField("AssociatedEvent").EqualTo(eventId).ToString();
        return csom.BulkDeleteListItems("Event Attendees", spWebUrl, query);
    }

}]);