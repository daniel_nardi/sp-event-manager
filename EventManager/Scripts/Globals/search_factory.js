﻿angular.module("EventManagerFactories")
.factory("SearchFactory", function () {
    var searchFactory = {};

    var ignoredTerms = ["a", "all", "also", "am", "an", "and", "any", "are", "as", "at", "be",
        "but", "by", "can", "did", "do", "does", "for", "from", "had", "has", "have", "here",
        "how", "i", "if", "in", "is", "it", "no", "not", "of", "on", "or", "so", "that", "the",
        "then", "there", "this", "to", "too", "up", "us", "use", "what", "when", "where", "who",
        "why", "you"];

    var searchFields = [
        { name: "Title", weight: 1000 },
        { name: "EventSummary", weight: 500 },
        { name: "Stakeholders", weight: 300 },
        { name: "ParentLocation", weight: 300 },
        { name: "EventType", weight: 300 },
        { name: "HostInformation", weight: 200 },
        { name: "VenueInformation", weight: 200 },
        { name: "EventPurpose", weight: 60 },
        { name: "Agenda", weight: 60 },
        { name: "Deliverables", weight: 50 },
        { name: "Schedule", weight: 50 }
    ]

    var compareFunc = function(a, b) {
        if (a.score < b.score)
            return 1;
        if (a.score > b.score)
            return -1;
        return 0;
    }

    searchFactory.IsolateTerms = function (terms) {
        terms = terms.toLowerCase();
        //first isolate terms and remove ignored
        var termsCol = _.reject(terms.split(/[ ,]+/), function (term) {
            return _.indexOf(ignoredTerms, term) > -1;
        });
        return termsCol;
    }

    searchFactory.ExecuteSearch = function (data, terms) {
        var score_objects = [];
        /*
        FIRST PASS:
            Grab an array of objects containing the item, along with the term hits for each field.
        */
        _.each(data, function (item) { //iterate over items
            var sItem = {item: item, fields: []}; //create object to hold item and scores against each weighting
            _.each(searchFields, function (field) { //iterate over searchfields
                var score = { name: field.name };//create inside sItem to hold each field name, and hits for each term
                var count = 0;
                _.each(terms, function (term) { //now iterate over terms
                    //search each field on the item for the term. If hit, increment the count
                    var regex = new RegExp('\\b' + term + '\\b');
                    //sometimes the stakeholders field comes back as an array. In this case, we convert back to delim string
                    if (field.name == "Stakeholders" && _.isArray(item[field.name])) {
                        item[field.name] = item[field.name].join(',');
                    }
                    //sometimes event types come back as an object, so fix that too
                    if (field.name == "EventType" && _.isObject(item[field.name])) {
                        item[field.name] = item[field.name].Title;
                    }
                    if (item[field.name] && item[field.name].toLowerCase().search(regex) > -1) { 
                        count++;
                    }
                })
                score.score = count * field.weight;
                sItem.fields.push(score)
            })
            var score = 0;
            _.each(sItem.fields, function (field) {
                //increase weighting if item is current or ended last 30 days
                if (item.EventEnd && moment(item.EventEnd).isAfter(moment().add(-30, "days"))) {
                    score += field.score * 2;
                }
                else score += field.score;
            })
            if (score > 0) {
                sItem.score = score;
                score_objects.push(sItem);
            }
        })

        /*
        SECOND PASS
            Order by score and return
        */
        return score_objects.sort(compareFunc);
    }

    return searchFactory;
});