angular.module('ui.calendar', [])
  .directive('uiCalendar', function () {
      return {
          restrict: 'A',
          link: function (scope, elm, attrs) {
              var cal = elm.find('.calendar');
              cal.fullCalendar(scope.calendarOptions)
              scope.$watch("events", function (events) {
                  cal.fullCalendar('removeEvents');
                  cal.fullCalendar('addEventSource', events);
                  cal.fullCalendar('rerenderEvents');
              }, true)
              scope.$watch("startDate", function (date) {
                  if (date) {
                      cal.fullCalendar('changeView', "basicDay");
                      cal.fullCalendar("gotoDate", date);
                  }
              })

              //with this you can handle the events that generated when we change the view i.e. Month, Week and Day
              scope.changeView = function (view, ev) {
                  ev.preventDefault();
                  currentView = view;
                  cal.fullCalendar('changeView', view);
              };
          }
      }
  });
