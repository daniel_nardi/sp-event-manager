﻿angular.module("EventManagerDirectives", ['ListServices', 'AngularSP', 'angular-growl'])
.directive('wysiwyg', function () {
    'use strict';
    return {
        transclude: true,
        restrict: 'EA',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            var options = angular.extend({
                semantic: false,
                closable: false
            }, scope.$eval(attrs.editorConfig));

            ngModelCtrl.$render = function () {
                angular.element(element).trumbowyg('html', ngModelCtrl.$viewValue);
            };

            angular.element(element).trumbowyg(options).on('tbwchange', function () {
                ngModelCtrl.$setViewValue(angular.element(element).trumbowyg('html'));
            }).on('tbwpaste', function () {
                ngModelCtrl.$setViewValue(angular.element(element).trumbowyg('html'));
            });
        }
    };
})
.directive("wizard", ['growl', function (growl) {
    return {
        restrict: "C",
        link: function (scope, element, attrs) {
            var step = function (action, step) {
                scope.$broadcast('show-errors-check-validity');
                if (element.find('.has-error:visible').length > 0) {
                    growl.warning("Validation errors detected. Rectify them to proceed.", {});
                    return;
                }

                var $active = element.parent().find('.wizard .nav-tabs li.active'),
                    activeStep = $active.data('step'),
                    nextStep = $active.next().data('step'),
                    prevStep = $active.prev().data('step'),
                    desiredStep;

                switch (action) {
                    case 'direct':
                        (step) ? desiredStep = step : 'step1';
                        break;
                    case 'next':
                        desiredStep = nextStep;
                        break;
                    case 'prev':
                        desiredStep = prevStep;
                        break;
                }

                if (desiredStep != activeStep) {
                    $active.removeClass('active');
                    element.parent().find('#' + activeStep).removeClass('active');
                    element.parent().find('li[data-step="' + desiredStep + '"]').addClass('active').removeClass('disabled');
                    element.parent().find('#' + desiredStep).addClass('active');
                }
            }
            /***************************
                Event Handlers
            ****************************/
            scope.nextStep = function (e) {
                step('next');
            }

            scope.prevStep = function (e) {
                step('prev');
            }

            scope.showStep = function (stepIndex, $event) {
                if ($($event.target).parents('li').hasClass('disabled')) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    return false;
                }
                else step('direct', stepIndex);
            }
        }
    }
}])
.directive("stakeholderManager", ["StakeholderService", "$timeout", function (stakeholders, $timeout) {
    return {
        restrict: "C",
        link: function (scope, element, attrs) {
            function Initiate() {
                element.tokenfield({
                    autocomplete: {
                        source: stakeholders.stakeholders,
                        delay: 100
                    },
                    showAutocompleteOnFocus: false,
                    beautify: false
                }).on('tokenfield:createtoken', function (event) {
                    //now make sure token doesn't already exist in list
                    var existingTokens = $(this).tokenfield('getTokens');
                    _.each(existingTokens, function (token) {
                        if (token === event.attrs.value) event.preventDefault();
                    });
                });
            }
            scope.$watch('loadComplete', function (val) {
                if (val) {
                    if (stakeholders.stakeholders.length == 0) {
                        stakeholders.GetStakeholders().then(function (data) {
                            if (data.length > 0) {
                                _.each(data, function (item) {
                                    stakeholders.stakeholders.push(item.Title);
                                })
                            }
                            Initiate();
                        })
                    }
                    else {
                        Initiate();
                    }
                }
            })
        }
    }
}])
.directive('attendeeForm', ['$confirm', 'SettingService', 'growl', 'AttendeeService',
    function ($confirm, settings, growl, attendees) {
    return {
        restrict: 'E',
        templateUrl: "../Scripts/Directives/Views/attendee-form.html",
        link: function (scope, element, attrs) {
            var attendeeFields = ["ID", "FirstName", "LastName", "JobTitle", "Company", "EmailAddress", "Manager", "RegisteredBy"];
            scope.editEnabled = false;
            var typeahead = $(element).find('input.geo-autocomplete').typeahead({
                minLength: 3,
                highlight: true
            },
            {
                name: "bleh",
                display: function (obj) {
                    return obj.FirstName + " " + obj.LastName;
                },
                source: function (val, sync, async) {
                    attendees.GetAttendeesByName(val).then(function (data) {
                        data = _.uniq(data, function (item) {
                            return item.FirstName && item.LastName && item.Company && item.JobTitle;
                        })
                        var objCol = [];
                        _.each(data, function (item) {
                            objCol.push(item);
                        });
                        async(objCol);
                    })
                },
                templates: {
                    empty: [
                      '<div class="empty-message">',
                        'No matching attendees',
                      '</div>'
                    ].join('\n'),
                    suggestion: function (obj) {
                        return "<div>" + obj.FirstName + " " + obj.LastName + " - " +
                            ((obj.JobTitle) ? obj.JobTitle + ", " : "") + ((obj.Company) ? obj.Company : "") + "</div>"
                    }
                }
            }).on('typeahead:selected', function (evt, item) {
                typeahead.typeahead('val', '');
                var data = _.pick(item, attendeeFields);
                data.ID = getNextId();
                scope.event.attendees.push(data);
                if (!scope.$$phase) scope.$apply();
            })

            //scope events
            scope.addAttendee = function () {
                scope.current = { ID: getNextId() };
                scope.editEnabled = true;
            }
            scope.removeAttendee = function (id) {
                $confirm({
                    text: 'Are you sure you wish to remove this attendee?',
                    title: 'Remove Attendee', ok: 'Yes', cancel: 'No'
                }).then(function () {
                    scope.event.attendees = _.reject(scope.event.attendees, function (attendee) {
                        return attendee.ID == id;
                    })
                })
            }
            scope.editAttendee = function (attendee) {
                scope.current = attendee;
                scope.editEnabled = true;
            }
            scope.pushCurrent = function () {
                scope.$broadcast('show-errors-check-validity');
                if (element.find('.has-error:visible').length > 0) {
                    growl.warning("Validation errors detected. Rectify them to proceed.", {});
                    return;
                }

                scope.event.attendees = _.reject(scope.event.attendees, function (attendee) {
                    return attendee.ID == scope.current.ID;
                })
                if (!scope.current.Company) {
                    scope.current.Company = "N/A";
                }
                scope.event.attendees.push(scope.current);
                scope.editEnabled = false;
            }
            scope.cancelAdd = function () {
                scope.editEnabled = false;
            }

            //local functions
            getNextId = function () {
                var ids = [], max = 1;
                //get next
                if (scope.event.attendees && scope.event.attendees.length > 0) {
                    _.each(scope.event.attendees, function (attendee) {
                        ids.push(attendee.ID)
                    })
                    max = Math.max.apply(Math, ids) + 1;
                }
                return max;
            }
        }
    };
}])