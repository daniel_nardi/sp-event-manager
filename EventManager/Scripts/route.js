﻿angular.module('eventProgrammeManager').config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: '../Scripts/Controllers/Dashboard/Views/my-dashboard.html',
        controller: 'MyDashboardController'
    })
    .when('/dashboard/my', {
        templateUrl: '../Scripts/Controllers/Dashboard/Views/my-dashboard.html',
        controller: 'MyDashboardController'
    })
    .when('/dashboard/my/notify/:message', {
        templateUrl: '../Scripts/Controllers/Dashboard/Views/my-dashboard.html',
        controller: 'MyDashboardController'
    })
    .when('/dashboard/admin', {
        templateUrl: '../Scripts/Controllers/Dashboard/Views/admin-dashboard.html',
        controller: 'AdminDashboardController'
    })
    .when('/events', {
        templateUrl: '../Scripts/Controllers/EventPlanner/Views/event-index.html',
        controller: 'EventIndexController'
    })
    .when('/events/filtered/:filter', {
        templateUrl: '../Scripts/Controllers/EventPlanner/Views/event-index.html',
        controller: 'EventIndexController'
    })
    .when('/events/:id/view', {
        templateUrl: '../Scripts/Controllers/EventPlanner/Views/event-view.html',
        controller: 'EventViewController'
    })
    .when('/events/new', {
        templateUrl: '../Scripts/Controllers/EventPlanner/Views/event-new.html',
        controller: 'EventCreateController'
    })
    .when('/events/new/parent/:parentEvent', {
        templateUrl: '../Scripts/Controllers/EventPlanner/Views/event-new.html',
        controller: 'EventCreateController'
    })
    .when('/events/new/date/:startDate', {
        templateUrl: '../Scripts/Controllers/EventPlanner/Views/event-new.html',
        controller: 'EventCreateController'
    })
    .when('/events/:id/edit', {
        templateUrl: '../Scripts/Controllers/EventPlanner/Views/event-edit.html',
        controller: 'EventEditController'
    })
    .when('/events/:id/edit/:parentEvent', {
        templateUrl: '../Scripts/Controllers/EventPlanner/Views/event-new.html',
        controller: 'EventEditController'
    })
    .when('/events/calendar', {
        templateUrl: '../Scripts/Controllers/EventPlanner/Views/event-calendar.html',
        controller: 'EventCalendarController'
    })
    .when('/events/calendar/:parentEvent', {
        templateUrl: '../Scripts/Controllers/EventPlanner/Views/event-calendar.html',
        controller: 'EventCalendarController'
    })
    .when('/search/:terms', {
        templateUrl: '../Scripts/Controllers/Search/Views/search-results.html',
        controller: 'SearchController'
    })
    .otherwise({
        redirectTo: '/dashboard/my'
    });
}]);