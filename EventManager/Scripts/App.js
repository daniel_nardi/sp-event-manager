﻿'use strict';
var rawUrl = location.protocol + '//' + location.host + location.pathname;
var spWebUrl = (_spPageContextInfo) ? _spPageContextInfo.webAbsoluteUrl : rawUrl.toLowerCase().replace("/pages/default.aspx", "");

//global jQuery functions
//highlights words. Use caution, call only on child elements
jQuery.fn.highlight = function (str, className) {
    var regex = new RegExp(str, "gi");
    return this.each(function () {
        $(this).contents().replaceWith(function () {
            return (this.nodeValue || "").replace(regex, function (match) {
                return "<span class=\"" + className + "\"><strong><u>" + match + "</u></strong></span>";
            });
        });
    });
};
// Prevent the backspace key from navigating back.
$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' &&
             (
                 d.type.toUpperCase() === 'TEXT' ||
                 d.type.toUpperCase() === 'PASSWORD' ||
                 d.type.toUpperCase() === 'FILE' ||
                 d.type.toUpperCase() === 'SEARCH' ||
                 d.type.toUpperCase() === 'EMAIL' ||
                 d.type.toUpperCase() === 'NUMBER' ||
                 d.type.toUpperCase() === 'DATE')
             ) ||
             d.tagName.toUpperCase() === 'TEXTAREA'
            ) {
            doPrevent = d.readOnly || d.disabled;
        }
        else if (d.tagName.toUpperCase() === 'DIV' && d.className === 'trumbowyg-editor') {
            doPrevent = false;
        }
        else {
            doPrevent = true;
        }
    }

    if (doPrevent) {
        event.preventDefault();
    }
});

var eventProgrammeManager = angular.module('eventProgrammeManager', [
            'ngRoute',
            'AngularSP',
            'EventManagerFactories',
            'ListServices',
            'EventManagerDirectives',
            'ngResource',
            'ui.People',
            'ez.modal',
            'ez.dropdown',
            'ez.transition',
            'ez.datetime',
            'ui.calendar',
            'angular-growl',
            'angular-confirm',
            'chart.js',
            'angularModalService',
            'ui.bootstrap.showErrors'
]).config(['growlProvider', function (growlProvider) {
    growlProvider.globalTimeToLive(5000);
    growlProvider.onlyUniqueMessages(true);
    growlProvider.globalDisableCountDown(true);
}]).run([
    "$rootScope",
    function ($rootScope) {
        spWebUrl = _spPageContextInfo.webAbsoluteUrl;

        //hack sp stuff
        $('#s4-titlerow').remove();
        $('.ms-core-deltaSuiteLinks').addClass('hidden-md').addClass('hidden-sm').addClass('hidden-xs');
        $('.ms-core-deltaSuiteBarRight').addClass('hidden-md').addClass('hidden-sm').addClass('hidden-xs');
        $('#s4-ribbonRow').addClass('hidden-md').addClass('hidden-sm').addClass('hidden-xs');

        //scroll to top
        $rootScope.$on('$routeChangeSuccess', function () {
            $('#main').animate({ scrollTop: 0 }, '0');
        });

        //populate event types URL
        $('.event-types').attr('href', spWebUrl + '/lists/event types');
    }
])

// This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
$(document).ready(function () {
    SP.SOD.executeOrDelayUntilScriptLoaded(initialise, "sp.js");
    function initialise() {
        //manually bootstrap this sucker
        angular.bootstrap(angular.element(".app-body")[0], ['eventProgrammeManager']);
    }
})


