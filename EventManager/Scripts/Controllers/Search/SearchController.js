﻿/*
TODO
    Filters:
        Current Events Only
        Parent Events Only
    Increase weighting for current?
*/

angular.module('eventProgrammeManager')
.controller('SearchController',
['$scope', '$location', '$routeParams', 'AngularSPCSOM', '$timeout', 'SearchFactory', 'EventService', 'TypeService',
function ($scope, $location, $routeParams, csom, $timeout, search, events, types) {
    console.log("SearchController hit, loading events");
    //load initial data
    var currentQueryData = [];
    $scope.initialSearch = $routeParams.terms;
    var searchTerms = search.IsolateTerms($routeParams.terms);
    _appGlobals.Loading(true);
    types.GetEventTypes().then(function (t) {
        types.eventTypes = t;
        GetItems();
    });

    function GetItems() {
        events.GetEvents().then(function (data) {
            console.log('Event load complete')
            var tmpData = [];
            _.each(data, function (itm) {
                itm.Image = _.find(types.eventTypes, function (type) {
                    return type.Title == itm.EventType;
                })
                if (itm.Image && itm.Image.ImageUrl) {
                    itm.Image = itm.Image.ImageUrl.get_url()
                }
                else itm.Image = "../Images/stock.png";

                tmpData.push(itm);
            });
            $scope.events = search.ExecuteSearch(tmpData, searchTerms);
            $timeout(function () {
                _.each(searchTerms, function (term) {
                    $('.search-highlight').highlight(term, 'text-danger');
                });
                _appGlobals.Loading(false);
            })
        }, function (error) {
            console.log(error.get_message())
        });
    }

    $scope.viewEvent = function (id) {
        $location.path('/events/' + id + '/view');
    }
}])
.controller('SearchBox',
['$scope', '$location',
function ($scope, $location) {
    $scope.searchTerms = '';

    $scope.runSearch = function (ev) {
        ev.preventDefault();
        $('.navbar-toggle').click();
        if ($scope.searchTerms) {
            $location.path('/search/' + $scope.searchTerms);
            $scope.searchTerms = '';
        }
    }
}])