﻿angular.module('eventProgrammeManager')
.controller('MyDashboardController',
['$scope', '$location', '$routeParams', 'AngularSPCSOM', '$timeout', 'EventService', 'growl',
function ($scope, $location, $routeParams, csom, $timeout, events, growl) {
    _appGlobals.Loading(true);
    events.GetEvents().then(function (data) {
        $scope.myDrafts = events.MyDraftEvents();
        $scope.myEvents = events.MyCurrentEvents();
        $scope.myApprovals = events.MyEventsToApprove();
        $scope.upcoming = events.MyUpcomingEvents();
        _appGlobals.Loading(false);
        if ($routeParams.message) {
            growl.success($routeParams.message, {});
        }
    })

    $scope.viewEvent = function (ev, id) {
        ev.preventDefault();
        $location.path('/events/' + id + '/view');
        if (!$scope.$$phase) $scope.$apply();
    }

    $scope.viewEvents = function (filter) {
        $location.path('/events/filtered/' + filter);
        if (!$scope.$$phase) $scope.$apply();
    }
}])
.controller('AdminDashboardController',
['$scope', '$location', '$routeParams', 'AngularSPCSOM', '$timeout', 'EventService',
function ($scope, $location, $routeParams, csom, $timeout, events) {
    console.log("AdminDashboardController hit, loading events");
    _appGlobals.Loading(true);
    events.GetEvents().then(function (data) {
        $scope.allDrafts = events.AllCurrentDraftEvents();
        $scope.allEvents = events.CurrentEvents();
        $scope.allApprovals = events.AllCurrentEventsInReview();
        $scope.upcoming = events.AllUpcomingEvents();

        $scope.currentMonth = moment().format("MMMM")
        var yearEvents = events.AllPublishedEventsThisYear();
        var monthEvents = events.AllPublishedEventsThisMonth();

        var mq = window.matchMedia("(max-width: 768px)");
        if (mq.matches) {
            $scope.monthLabels = [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sept",
            "Oct",
            "Nov",
            "Dec"
            ];
        }
        else $scope.monthLabels = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ];
        $scope.monthSeries = ["Published Events"];
        $scope.monthData = [[
            (yearEvents.monthly["January"]) ? yearEvents.monthly["January"] : 0,
            (yearEvents.monthly["February"]) ? yearEvents.monthly["February"] : 0,
            (yearEvents.monthly["March"]) ? yearEvents.monthly["March"] : 0,
            (yearEvents.monthly["April"]) ? yearEvents.monthly["April"] : 0,
            (yearEvents.monthly["May"]) ? yearEvents.monthly["May"] : 0,
            (yearEvents.monthly["June"]) ? yearEvents.monthly["June"] : 0,
            (yearEvents.monthly["July"]) ? yearEvents.monthly["July"] : 0,
            (yearEvents.monthly["August"]) ? yearEvents.monthly["August"] : 0,
            (yearEvents.monthly["September"]) ? yearEvents.monthly["September"] : 0,
            (yearEvents.monthly["October"]) ? yearEvents.monthly["October"] : 0,
            (yearEvents.monthly["November"]) ? yearEvents.monthly["November"] : 0,
            (yearEvents.monthly["December"]) ? yearEvents.monthly["December"] : 0
        ]]
        $scope.typeLabels = [];
        $scope.typeData = [];
        $scope.statusLabels = [];
        $scope.statusData = [];

        _.each(monthEvents.type, function (val, key) {
            $scope.typeLabels.push(key);
            $scope.typeData.push(val);
        })

        _.each(monthEvents.status, function (val, key) {
            $scope.statusLabels.push(key);
            $scope.statusData.push(val);
        })

        _appGlobals.Loading(false);
    })

    $scope.viewEvents = function (filter) {
        $location.path('/events/filtered/' + filter);
        if (!$scope.$$phase) $scope.$apply();
    }
}])