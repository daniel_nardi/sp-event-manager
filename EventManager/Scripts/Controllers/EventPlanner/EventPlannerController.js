﻿angular.module('eventProgrammeManager')
.controller('EventIndexController',
['$scope', '$location', '$routeParams', 'AngularSPCSOM', '$timeout', 'EventService', 'SettingService', 'TypeService',
function ($scope, $location, $routeParams, csom, $timeout, events, settings, types) {
    _appGlobals.Loading(true);
    //load initial data
    var currentQueryData = [];
    types.GetEventTypes().then(function (t) {
        events.GetEvents().then(function (data) {
            if ($routeParams.filter) {
                var filter = $routeParams.filter;
                if (filter.indexOf("-") == -1) {
                    data = events[filter]().events
                }
                else data = events[filter.split('-')[0]]()[filter.split('-')[1]];
            }
            else {
                data = events.CurrentEventsInvolvingMe();
            }
            GetData(data);
        });
    })

    /*******************************
        Controller-Scoped Functions
    *******************************/
    function GetData(data) {
        var locations = [], evTypes = [], tmpData = [];
        _.each(data, function (itm) {
            itm.Image = _.find(types.eventTypes, function (type) {
                return type.Title == itm.EventType;
            })
            if (itm.Image && itm.Image.ImageUrl) {
                itm.Image = itm.Image.ImageUrl.get_url()
            }
            else itm.Image = "../Images/stock.png";

            tmpData.push(itm);
            locations.push(itm.ParentLocation);
            evTypes.push(itm.EventType);
        });
        $scope.locations = _.uniq(locations);
        $scope.eventTypes = _.uniq(evTypes);
        $scope.events = currentQueryData = tmpData;
        $timeout(function () {
            _appGlobals.Loading(false);
        })
    }


    /***************************
        Event Handlers
    ****************************/
    $scope.clearFilter = function () {
        $scope.events = currentQueryData;
    }
    $scope.locationFilter = function (location) {
        $scope.events = _.filter(currentQueryData, function (ev) {
            return ev.ParentLocation == location;
        });
    }
    $scope.typeFilter = function (type) {
        $scope.events = _.filter(currentQueryData, function (ev) {
            return ev.EventType == type;
        });
    }
    $scope.viewEvent = function (id) {
        $location.path('/events/' + id + '/view');
    }
    $scope.filterResults = function (type) {
        currentQueryType = type;
        switch (type) {
            case 'all':
                GetData(events.CurrentEvents().events);
                break;
            case 'upcoming':
                GetData(events.AllUpcomingEvents().events);
                break;
            case 'my':
                GetData(events.CurrentEventsInvolvingMe());
                break;
            case 'previous':
                GetData(events.PreviousEvents());
                break;
        }
    }
}])

.controller('EventViewController',
['$scope', '$location', '$routeParams', 'AngularSPCSOM', '$timeout',
    'growl', '$confirm', 'EventService', 'SettingService', 'AttendeeService', 'ModalService',
function ($scope, $location, $routeParams, csom, $timeout, growl, $confirm, events, settings, attendees, modal) {
    _appGlobals.Loading(true);

    events.GetEvents().then(function (d) {
        var evnt = events.GetEvent($routeParams.id);
        if (evnt.Stakeholders) {
            if (!_.isArray(evnt.Stakeholders)) {
                evnt.Stakeholders = evnt.Stakeholders.split(',');
            }
        }
        if (evnt.NominatedApprovers) {
            evnt.IsApprover = _.some(evnt.NominatedApprovers, function (approver) {
                return approver.get_lookupId() == _spPageContextInfo.userId;
            })
        }
        if (_.isObject(evnt.EventType)) {
            evnt.EventType = evnt.EventType.Title;
        }
        $scope.event = evnt;

        if (evnt.ParentEvent) {
            $scope.parentEventObj = events.GetEvent(evnt.ParentEvent);
        }
        else {
            $scope.event.SubEvents = events.GetSubEvents($routeParams.id);
        }

        attendees.GetAttendees(evnt.ID).then(function (data) {
            $scope.event.attendees = data;
            $timeout(function () {
                $scope.loadComplete = true;
                $('.attend-table').paging({ limit: 5 });
                _appGlobals.Loading(false);
            })
        })

    }, function (error) {
        console.log(error.get_message())
    });

    $scope.attendeeDetails = function (user) {
        modal.showModal({
            templateUrl: "../Scripts/Controllers/EventPlanner/Views/attendee-info-modal.html",
            controller: "AttendeeInfoModal",
            inputs: {
                user: user
            }
        }).then(function (modal) {
            modal.element.modal();
        });
    }

    $scope.toggleAttendance = function(user){
        attendees.UpdateAttendance(user.ID, user.AttendanceConfirmed).then(function () {
            growl.success("Attendee confirmation updated successfully.", {})
        })
    }

    $scope.removeEvent = function (ev) {
        ev.preventDefault();
        $confirm({
            text: 'Are you sure you want to delete this Event?',
            title: 'Delete Event', ok: 'Yes', cancel: 'No'
        })
        .then(function () {
            events.RemoveEvent($scope.event.ID).then(function (event) {
                _appGlobals.Loading(false);
                $location.path('/dashboard/my/notify/' + "Event removed successfully");
                if (!$scope.$$phase) $scope.$apply();
            });
        });
    }

    $scope.submitEvent = function (ev) {
        ev.preventDefault();
        $confirm({
            text: 'Are you sure you want to submit the Event for approval?',
            title: 'Submit for Approval', ok: 'Yes', cancel: 'No'
        })
        .then(function () {
            if ($scope.event.NominatedApprovers) {
                $scope.event.EventStatus = "Pending Approval";
                events.UpdateEvent($scope.event.ID, { EventStatus: "Pending Approval" }).then(function (event) {
                    settings.GetSettings().then(function (_settings) {
                        var emailBody = "Hello, <br/><br/> Your approval has been requested for the Event <b>" + event.Title +
                                "</b>. <br/><br/>Please review the Event <a href='" + spWebUrl + 
                                "/pages/default.aspx#events/" + event.ID + "/view'>here</a>"
                                
                        _.each($scope.event.NominatedApprovers, function (approver) {
                            _appGlobals.SendEmail(_settings.FromEmail.Value, approver.get_email(), "Event Approval", emailBody)
                        })

                        _appGlobals.Loading(false);
                    })
                });
            }
            else growl.warning("You cannot submit an Event for approval without nominating approvers", {});
        })
    }

    $scope.unlockEvent = function (ev) {
        ev.preventDefault();
        $confirm({
            text: 'Are you sure you want to unlock the Event for editing?',
            title: 'Unlock Event', ok: 'Yes', cancel: 'No'
        })
        .then(function () {
            $scope.event.EventStatus = "Draft";
            events.UpdateEvent($scope.event.ID, { EventStatus: "Draft" }).then(function (event) {
                _appGlobals.Loading(false);
            });
        });
    }

    $scope.approveEvent = function (ev, approved) {
        ev.preventDefault();
        var body, author = $scope.event.Author.get_lookupValue();
        if (approved) {
            $scope.event.EventStatus = "Published";
            body = "Hello " + author + ",<br/><br/> Your event, <b>" + $scope.event.Title +
                        "</b> has been Approved. <br/><br/>" +
                        "Please review the Event <a href='" + spWebUrl +
                        "/pages/default.aspx#events/" + $scope.event.ID + "/view'>here</a>";
            events.UpdateEvent($scope.event.ID, { EventStatus: $scope.event.EventStatus }).then(function (event) {
                settings.GetSettings().then(function (_settings) {
                    _appGlobals.SendEmail(_settings.FromEmail.Value, author, "Event " + event.EventStatus, body)
                    _appGlobals.Loading(false);
                })
            });
        }
        else {
            modal.showModal({
                templateUrl: "../Scripts/Controllers/EventPlanner/Views/reject-modal.html",
                controller: "RejectModal",
                inputs: {
                    title: "Reject Event"
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    if (!result) {
                        return false;
                    }
                    else {
                        $scope.event.RejectMessage = result;
                        $scope.event.EventStatus = "Rejected";
                        body = "Hello " + author + "," +
                            "<br/><br/> Your event, <b>" + $scope.event.Title +
                            "</b> has been Rejected with the following justification: <br/><br/>" +
                            "<p>" + (($scope.event.RejectMessage) ? $scope.event.RejectMessage : "No justification profided") +
                            "</p><br/><br/>Please review the Event <a href='" + spWebUrl +
                            "/pages/default.aspx#events/" + $scope.event.ID + "/view'>here</a>";
                        events.UpdateEvent($scope.event.ID, { 
                            EventStatus: $scope.event.EventStatus, 
                            RejectMessage: $scope.event.RejectMessage
                        }).then(function (event) {
                            settings.GetSettings().then(function (_settings) {
                                _appGlobals.SendEmail(_settings.FromEmail.Value, author, "Event " + event.EventStatus, body)
                                _appGlobals.Loading(false);
                            })
                        });
                    }
                });
            });
        }
    }
}])
.filter('rawHtml', ['$sce', function ($sce) {
    return function (val) {
        return $sce.trustAsHtml(val);
    };
}])
.controller('RejectModal', ['$scope', '$element', 'title', 'close', 
function($scope, $element, title, close) {
    $scope.message = null;
    $scope.title = title;
  
    //  This close function doesn't need to use jQuery or bootstrap, because
    //  the button has the 'data-dismiss' attribute.
    $scope.save = function() {
        close($scope.message, 500); // close, but give 500ms for bootstrap to animate
    };

    //  This cancel function must use the bootstrap, 'modal' function because
    //  the doesn't have the 'data-dismiss' attribute.
    $scope.cancel = function() {

        //  Manually hide the modal.
        $element.modal('hide');
    
        //  Now call close, returning control to the caller.
        close(false, 500); // close, but give 500ms for bootstrap to animate
    };
}])
.controller('AttendeeInfoModal', ['$scope', '$element', 'user', 'close',
function ($scope, $element, user, close) {
    $scope.currentAttendee = user;

    //  This cancel function must use the bootstrap, 'modal' function because
    //  the doesn't have the 'data-dismiss' attribute.
    $scope.cancel = function () {

        //  Manually hide the modal.
        $element.modal('hide');

        //  Now call close, returning control to the caller.
        close(false, 500); // close, but give 500ms for bootstrap to animate
    };
}])


.controller('EventCreateController',
['$scope', '$location', '$routeParams', 'AngularSPCSOM', 'StakeholderService', 'TypeService', '$timeout', 'EventService', 'AttendeeService',
function ($scope, $location, $routeParams, csom, StakeholderService, types, $timeout, events, attendees) {
    _appGlobals.Loading(true);
    //declare object & settings
    $scope.event = {attendees:[]};
    $scope.eventTypes = [];
    if ($routeParams.parentEvent) $scope.event.ParentEvent = $routeParams.parentEvent;
    if ($routeParams.startDate) $scope.event.EventStart = moment($routeParams.startDate).format();
    $scope.event.EventStatus = "Draft"

    $scope.trumboConf = {
        fullscreenable: false
    };

    types.GetEventTypes().then(function (t) {
        $scope.eventTypes = t;

        $timeout(function () {
            $scope.loadComplete = true;
            _appGlobals.Loading(false);
        });
    })

    $scope.saveEvent = function () {
        _appGlobals.Loading(true, "Saving your Event, one moment...");

        //get proper date values for saving
        if ($scope.event.EventStart) {
            $scope.event.EventStart = moment(new Date($scope.event.EventStart)).format()
        }
        if ($scope.event.EventEnd) {
            $scope.event.EventEnd = moment(new Date($scope.event.EventEnd)).format()
        }

        //get proper event type for saving
        if ($scope.event.EventType) {
            $scope.event.EventType = $scope.event.EventType.Title;
        }

        //get proper user values for saving
        if (_.isArray($scope.event.LeadParticipant) && $scope.event.LeadParticipant.length == 0) {
            $scope.event.LeadParticipant = '';
        }
        else {
            if ($scope.event.LeadParticipant) {
                var lp = $scope.event.LeadParticipant;
                (_.isArray(lp)) ? lp = lp[0] : lp = lp;
                var saveArr = [];
                if (lp.get_lookupId) {
                    saveArr.push(lp.get_lookupId() + ';#' + lp.get_lookupValue())
                }
                else saveArr.push(lp.SaveString);
                $scope.event.LeadParticipant = saveArr.join(";#");
            }
            else $scope.event.LeadParticipant = '';
        }
        
        if ($scope.event.AdditionalParticipants) {
            if ($scope.event.AdditionalParticipants.length > 0) {
                var saveArr = [];
                _.each($scope.event.AdditionalParticipants, function (p) {
                    if (p.get_lookupId) {
                        saveArr.push(p.get_lookupId() + ';#' + p.get_lookupValue())
                    }
                    else saveArr.push(p.SaveString);
                })
                $scope.event.AdditionalParticipants = saveArr.join(";#");
            }
            else $scope.event.AdditionalParticipants = '';
        }
        if ($scope.event.NominatedApprovers) {
            if ($scope.event.NominatedApprovers.length > 0) {
                var saveArr = [];
                _.each($scope.event.NominatedApprovers, function (p) {
                    if (p.get_lookupId) {
                        saveArr.push(p.get_lookupId() + ';#' + p.get_lookupValue())
                    }
                    else saveArr.push(p.SaveString);
                })
                $scope.event.NominatedApprovers = saveArr.join(";#");
            }
            else $scope.event.NominatedApprovers = '';
        }

        //Prep attendees
        var saveAttendees = $scope.event.attendees;
        $scope.event = _.omit($scope.event, "attendees");

        events.AddEvent($scope.event).then(function (event) {
            
            StakeholderService.AddNewStakeholders($scope.event.Stakeholders, ",");

            var saveArr = [];
            _.each(saveAttendees, function (attend) {
                saveArr.push(_.extend(_.omit(attend, ['ID', '$$hashKey']), { AssociatedEvent: event.ID }))
            });
            if (saveArr.length > 0) {
                attendees.BulkAddAttendees(saveArr).then(function () {
                    _appGlobals.Loading(false);
                    $location.path('/events/' + event.ID + '/view');
                })
            }
            else {
                _appGlobals.Loading(false);
                $location.path('/events/' + event.ID + '/view');
            }
        });
    };
}])

.controller('EventEditController',
['$scope', '$location', '$routeParams', 'AngularSPCSOM', 'StakeholderService', 'TypeService', '$timeout', 'EventService', 'AttendeeService',
function ($scope, $location, $routeParams, csom, StakeholderService, types, $timeout, events, attendees) {
    _appGlobals.Loading(true);

    types.GetEventTypes().then(function (t) {
        $scope.eventTypes = t;
        events.GetEvents().then(function (d) {
            var evnt = events.GetEvent($routeParams.id);
            if (evnt.EventStart) evnt.EventStart = moment(evnt.EventStart).format();
            if (evnt.EventEnd) evnt.EventEnd = moment(evnt.EventEnd).format();

            //assign event
            $scope.event = evnt;

            if (evnt.EventType) {
                $scope.event.EventType = _.find($scope.eventTypes, function (type) {
                    return evnt.EventType == type.Title;
                })
            }

            $scope.trumboConf = {
                fullscreenable: false
            };

            attendees.GetAttendees(evnt.ID).then(function (data) {
                $scope.event.attendees = data;
                $timeout(function () {
                    $scope.loadComplete = true;
                    _appGlobals.Loading(false);
                })
            })
        });
    });

    $scope.saveEvent = function () {
        _appGlobals.Loading(true, "Saving your Event, one moment...");
        var eventId = $scope.event.ID;
        //Prep attendees
        var saveAttendees = $scope.event.attendees;
        $scope.event = _.pick($scope.event, _appGlobals.EventFields)

        $scope.event.EventStatus = "Draft";

        //get proper date values for saving
        if ($scope.event.EventStart) {
            $scope.event.EventStart = moment(new Date($scope.event.EventStart)).format()
        }
        if ($scope.event.EventEnd) {
            $scope.event.EventEnd = moment(new Date($scope.event.EventEnd)).format()
        }

        //get proper event type for saving
        if ($scope.event.EventType) {
            $scope.event.EventType = $scope.event.EventType.Title;
        }

        //get proper user values for saving
        if (_.isArray($scope.event.LeadParticipant) && $scope.event.LeadParticipant.length == 0) {
            $scope.event.LeadParticipant = '';
        }
        else {
            if ($scope.event.LeadParticipant) {
                var lp = $scope.event.LeadParticipant;
                (_.isArray(lp)) ? lp = lp[0] : lp = lp;
                var saveArr = [];
                if (lp.get_lookupId) {
                    saveArr.push(lp.get_lookupId() + ';#' + lp.get_lookupValue())
                }
                else saveArr.push(lp.SaveString);
                $scope.event.LeadParticipant = saveArr.join(";#");
            }
            else $scope.event.LeadParticipant = '';
        }

        if ($scope.event.AdditionalParticipants) {
            if ($scope.event.AdditionalParticipants.length > 0) {
                var saveArr = [];
                _.each($scope.event.AdditionalParticipants, function (p) {
                    if (p.get_lookupId) {
                        saveArr.push(p.get_lookupId() + ';#' + p.get_lookupValue())
                    }
                    else saveArr.push(p.SaveString);
                })
                $scope.event.AdditionalParticipants = saveArr.join(";#");
            }
            else $scope.event.AdditionalParticipants = '';
        }
        if ($scope.event.NominatedApprovers) {
            if ($scope.event.NominatedApprovers.length > 0) {
                var saveArr = [];
                _.each($scope.event.NominatedApprovers, function (p) {
                    if (p.get_lookupId) {
                        saveArr.push(p.get_lookupId() + ';#' + p.get_lookupValue())
                    }
                    else saveArr.push(p.SaveString);
                })
                $scope.event.NominatedApprovers = saveArr.join(";#");
            }
            else $scope.event.NominatedApprovers = '';
        }

        if (_.isArray($scope.event.Stakeholders)) {
            $scope.event.Stakeholders = $scope.event.Stakeholders.join(',')
        }

        events.UpdateEvent(eventId, $scope.event).then(function (event) {
            
            StakeholderService.AddNewStakeholders($scope.event.Stakeholders, ",");
            var attArr = [];
            var attendeeFields = ["FirstName", "LastName", "JobTitle", "Company",
                "EmailAddress", "Manager", "RegisteredBy", "AttendanceConfirmed"];
            _.each(saveAttendees, function (attend) {
                attArr.push(_.extend(_.pick(attend, attendeeFields), { AssociatedEvent: event.ID }))
            });
            attendees.BulkRemoveAttendees(event.ID).then(function () {
                if (attArr.length > 0) {
                    attendees.BulkAddAttendees(attArr).then(function () {
                        _appGlobals.Loading(false);
                        $location.path('/events/' + event.ID + '/view');
                    })
                }
                else {
                    _appGlobals.Loading(false);
                    $location.path('/events/' + event.ID + '/view');
                }
            })
            
        });
    };
}]);