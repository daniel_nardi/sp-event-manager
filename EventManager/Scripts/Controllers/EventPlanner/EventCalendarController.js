﻿/*
TODO:
    Implement calendar view for single event and sub-events
    Make buttons responsive & remove list of events in mobile view
    Implement click & maybe click drag functionality for new event creation
*/

angular.module('eventProgrammeManager').controller('EventCalendarController',
['$scope', '$location', '$routeParams', 'AngularSPCSOM', '$timeout', 'EventService',
function ($scope, $location, $routeParams, csom, $timeout, events) {
    $scope.events = [];
    /* config object */
    $scope.calendarOptions = {
        eventSources: [
            { events: $scope.events }
        ],
        editable: true,
        header: {
            left: 'title',
            center: '',
            right: 'today,prev,next'
        },
        allDaySlot: false,
        dayClick: function (date, allDay, jsEvent, view) {
            $location.path('/events/new/date/' + date.format());
            if (!$scope.$$phase) $scope.$apply()
        },
        eventDrop: $scope.alertOnDrop,
        eventResize: $scope.alertOnResize,
        eventClick: $scope.eventClick,
        viewRender: $scope.renderView
    };

    events.GetEvents().then(function (data) {
        //load initial data
        if ($routeParams.parentEvent) {
            fetchEvents(events.EventAndSubs($routeParams.parentEvent));
            $scope.startDate = moment(events.GetEvent($routeParams.parentEvent).EventStart);
        }
        else {
            fetchEvents(events.CurrentEventsInvolvingMe());
        }
    })

    /*******************************
        Controller-Scoped Functions
    *******************************/
    function fetchEvents(data) {
        _appGlobals.Loading(true);
        $scope.events = [];
        if (data) {
            for (var i = 0; i < data.length; i++) {
                $scope.events[i] = {
                    id: data[i].ID,
                    title: data[i].Title,
                    start: new Date(data[i].EventStart),
                    end: new Date(data[i].EventEnd),
                    url: "#events/" + data[i].ID + "/view",
                    allDay: false
                };
                if (data[i].ParentEvent) {
                    $scope.events[i].backgroundColor = "#3c763d";
                }
            }
        }
        $timeout(function () {
            _appGlobals.Loading(false);
        })
    }

    /***************************
        Event Handlers
    ****************************/

    $scope.filterEvents = function (context, ev) {
        ev.preventDefault();
        switch (context) {
            case "all":
                fetchEvents(events.CurrentEvents().events);
                break;
            case "my":
                fetchEvents(events.CurrentEventsInvolvingMe());
                break;
        }

    }

}])
