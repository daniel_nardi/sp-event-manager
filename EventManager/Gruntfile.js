module.exports = function (grunt) {
    var jsFiles = [
        "Scripts/Plugins/chart.js",
        "Scripts/Plugins/angular/angular.min.js",
        "Scripts/Plugins/angular/angular-sp.js",
        "Scripts/Plugins/angular/angular-route.min.js",
        "Scripts/Plugins/angular/angular-resource.js",
        "Scripts/Plugins/angular/angular-chart.js",
        "Scripts/Plugins/angular/angular-validate.js",
        "Scripts/Plugins/holdon.min.js",
        "Scripts/Plugins/bootstrap/bootstrap.min.js",
        "Scripts/Plugins/moment.min.js",
        "Scripts/Plugins/underscore.js",
        "Scripts/Plugins/camljs.js",
        "Scripts/Plugins/tokenfield/tokenfield.js",
        "Scripts/Plugins/typeahead.js",
        "Scripts/Plugins/pager.js",
        "Scripts/Plugins/jqueryui/jqueryui.js",
        "Scripts/Plugins/ezmodal/ez-modal.js",
        "Scripts/Plugins/ez-dropdown.js",
        "Scripts/Plugins/ez-transition.js",
        "Scripts/Plugins/angular/angular-confirm.js",
        "Scripts/Plugins/datetimepicker/datetimepicker.js",
        "Scripts/Plugins/trumbowyg/trumbowyg.min.js",
        "Scripts/Plugins/fullcalendar/fullcalendar.js",
        "Scripts/Plugins/growl/growl.js",
        //"Scripts/Fixtures/dummyData.js",
        "Scripts/Globals/helpers.js",
        "Scripts/Globals/caml_factory.js",
        "Scripts/Globals/search_factory.js",
        "Scripts/Globals/stakeholder_service.js",
        "Scripts/Globals/support_services.js",
        "Scripts/Globals/event_service.js",
        "Scripts/Globals/stakeholder_service.js",
        "Scripts/Globals/attendee_service.js",
        "Scripts/Directives/angular-ppicker.js",
        "Scripts/Directives/datepicker.js",
        "Scripts/Directives/calendar.js",
        "Scripts/Directives/EventManagerDirectives.js",
        "Scripts/App.js",
        "Scripts/Controllers/EventPlanner/EventPlannerController.js",
        "Scripts/Controllers/EventPlanner/EventCalendarController.js",
        "Scripts/Controllers/Search/SearchController.js",
        "Scripts/Controllers/Dashboard/DashboardController.js",
        "Scripts/route.js"
    ];
    var cssFiles = [
        "Scripts/Plugins/bootstrap/bootstrap.min.css",
        "Scripts/Plugins/bootstrap/bootstrap-theme.min.css",
        "Scripts/Plugins/trumbowyg/ui/trumbowyg.min.css",
        "Scripts/Plugins/tokenfield/css/tokenfield.css",
        "Scripts/Plugins/datetimepicker/datetimepicker.css",
        "Scripts/Plugins/jqueryui/jqueryui.css",
        "Scripts/Plugins/fullcalendar/fullcalendar.min.css",
        "Scripts/Plugins/growl/growl.css",
        "Scripts/Plugins/angular/angular-chart.css",
        "Content/App.css"
    ]
    grunt.initConfig({
        uglify: {
            demo: {
                files: {
                    'Scripts/production.js': jsFiles
                }
            }
        },
        closurecompiler: {
            minify: {
                files: {
                    "Scripts/production.js": jsFiles
                },
                options: {
                    "compilation_level": "SIMPLE_OPTIMIZATIONS",
                    "max_processes": 5,
                    "banner": "/* SP Event Manager by Dan Nardi,  http://www.dnardi.com. This banner must not be removed. */"
                }
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    "Content/production.css": cssFiles
                }
            }
        }
    });
    grunt.loadNpmTasks("grunt-closurecompiler");
    //grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.registerTask("build", ["closurecompiler", "cssmin"]);

};